# Changelog

---

## [v2.0.7](https://gitlab.com/pd-packages/package-tool/-/tags/v2.0.7)

### Fixed

- Newtonsoft Json dependency

---

## [v2.0.6](https://gitlab.com/pd-packages/package-tool/-/tags/v2.0.6)

### Fixed

- Remove `npm install` from CI job template

---

## [v2.0.5](https://gitlab.com/pd-packages/package-tool/-/tags/v2.0.5)

### Fixed

- Remove `.idea` folder from package
- Package repository url in README.md

---

## [v2.0.4](https://gitlab.com/pd-packages/package-tool/-/tags/v2.0.4)

### Fixed

- Remove `npm install` from CI job for avoid unresolved dependencies conflicts
- Package initializer window size and make size flexible

---

## [v2.0.3](https://gitlab.com/pd-packages/package-tool/-/tags/v2.0.3)

### Fixed

- Links to readme, changelog and license files

---

## [v2.0.2](https://gitlab.com/pd-packages/package-tool/-/tags/v2.0.2)

### Changed

- Migrate to GitLab
- New package name `com.playdarium.unity.package-tools`
- CI file generation

---

## [v1.0.3](https://github.com/playdarium/package-tool/releases/tag/v1.0.3)

### Changed

- Fix generated package folder path

---

## [v1.0.2](https://github.com/playdarium/package-tool/releases/tag/v1.0.2)

### Added

- `ExampleProject` for show how work adding samples to package

### Changed

- Update changelog template

---

## [v1.0.1](https://github.com/playdarium/package-tool/releases/tag/v1.0.1)

### Added

- Init package fields validation
- CIUtils script for generate package and prepare dll configuration
- Auto generate `Documentation~` and `Samples~` folders when init package

### Changed

- Changed namespace from `Package.PackageTool` to `Playdarium.PackageTool`

---

## [v1.0.0](https://github.com/playdarium/package-tool/releases/tag/v1.0.0)

### Added

- Export `Documentation~` folder to package manifest
- Export samples to package manifest
- Fields for copy files README, CHANGELOG, LICENSE
- Auto filled url for documentation, changelog and license in `package.json`

### Changed

- Make readable json format in `package.json` file

---

## [v1.0.0](https://github.com/playdarium/package-tool/releases/tag/v1.0.0)

### Added

- For new features.

### Changed

- For changes in existing functionality.

### Deprecated

- For soon-to-be removed features.

### Removed

- For now removed features.

### Fixed

- For any bug fixes.

### Security

- In case of vulnerabilities.